const TelegramBot = require('node-telegram-bot-api');
const cron = require('node-cron');
const { ReplyManager, OperationManager } = require("node-telegram-operation-manager");
const db = require('./db');
const debug = require('./utils');
const init = require('./updates');
const TOKEN = '1216202410:AAEBfh1LzvwiXSpV19P3_curOoRLD8uX7GE';
const baseUrlFromParse = 'https://m.kolesa.kz';
const opm = new OperationManager();
const reply = new ReplyManager();

require('https').createServer().listen(process.env.PORT || 5000).on('request', function(req, res){
    res.end('')
})

global.bot = new TelegramBot(TOKEN, {
    polling: {
        interval: 500,
        autoStart: true,
        params: {
            timeout: 10
        }
    }
});

bot.onText(/\/start/, msg => {
    const { id } = msg.chat;
    db.createUser(id);
});

bot.onText(/\/add (.+)/, (msg, [source, match]) => {
    const { id } = msg.chat;
    const url = match.replace(/"/g,"");
    db.saveUrl(id, url).then(() => {
        bot.sendMessage(id, 'Ссылка добавлена!');
    }).catch(() => {
        bot.sendMessage(id, 'Ссылка уже отслеживается!');
    });
});

bot.onText(/\/list/, async (msg) => {
    const { id } = msg.chat;
    const user = await db.getUser(id);
    let links = user.links;
    if(links.length) {
        let txt = 'Список отслеживаемых ссылок.\n';
        for(let i = 0; i <= links.length - 1; i++) {
            txt += `${i + 1} - ${links[i]}\n`
        }
        txt += 'Чтобы удалить ссылку используйте команду /delete'
        bot.sendMessage(id, txt, {
            disable_web_page_preview: true
        });
    } else {
        bot.sendMessage(id, 'Вы ничего не отслеживаете.\nЧтобы добавить ссылку используйте команду \n/add https://m.kolesa.kz/', {
            disable_web_page_preview: true
        })
    }
    
});

bot.onText(/\/delete/, async (msg) => {
    const { id } = msg.chat;
    if (opm.hasReachedMaximum(id)) {
		bot.sendMessage(id, "Невозможно выполнить /delete. Еще одна операция выполняется.");
		return false;
	}
    const user = await db.getUser(id);
    let links = user.links;
    if(links.length) {
        let txt = 'Отправьте порядковый номер удаляемой ссылки.\n';
        for(let i = 0; i <= links.length - 1; i++) {
            txt += `${i + 1} - ${links[i]}\n`
        }
        opm.register(id, 'delete', () => {
            try {
                bot.sendMessage(id, txt, {
                    disable_web_page_preview: true
                });
                reply.register(id, (data) => {
                    console.log()
                    if (!isNumeric(data.text) || +data.text == 0 || +data.text > links.length) {
                        bot.sendMessage(id, `Введите число соотвествующее порядковому номеру.\nвы ввели ${data.text}`);
                        return { repeat: true };
                    }
                    db.deleteUrl(id,+data.text);
                    bot.sendMessage(id, 'Ссылка удалена!');
                    opm.end(id, 'delete');
                });
            } catch (error) {
                
            }
        })
    } else {
        bot.sendMessage(id, 'Вы ничего не отслеживаете.')
    }
});

// bot.onText(/\/test/, async (msg) => {
//     init();
// });


bot.on("message", (msg) => {
	if (!hasEntity("bot_command", msg.entities) && opm.hasActive(msg.from.id) && reply.expects(msg.from.id)) {
		reply.execute(msg.from.id, { text: msg.text, entities: msg.entities });
	}
});

cron.schedule('*/6 * * * *', function() {
    init();
});

bot.on("polling_error", (err) => console.log(err));


function isNumeric(value) {
    return /^\d+$/.test(value);
}

function hasEntity(entity, entities) {
	if (!entities || !entities.length) {
		return false;
	}

	return entities.some(e => e.type === entity);
}