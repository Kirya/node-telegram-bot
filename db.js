const datastore = require('nedb-promise');
let nedb = datastore({
    filename: 'base',
    autoload: true
 })

const db = {
    async createUser(id) {
        const user = await this.getUser(id);
        console.log(user)
        if(!user) {
            nedb.insert({
                _id: id,
                links: []
            });
        }
    },
    async saveUrl(id, url) {
        const user = await this.getUser(id);
        const links = user.links;
        return new Promise((resolve, reject) => {
            if(!links.includes(url)) {
                nedb.update({ _id: id }, { $push: { links: url } }, {});
                resolve()
            } else {
                reject()
            }
        })
    },
    async deleteUrl(id, index) {
        const user = await this.getUser(id);
        let links = user.links;
        links.splice(index - 1, index);
        nedb.update({ _id: id }, { links: links }, {});
    },
    async getUser(id) {
        let user = await nedb.findOne({_id: id});;
        return user;
    },
    async getUsers() {
        let users = await nedb.find({});
        return users;
    }
}

module.exports = db;