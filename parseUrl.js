const rp = require('request-promise');
const cheerio = require('cheerio');

function parseUrl(url) {
    const options = {
        uri: url,
        transform: function (body) {
            return cheerio.load(body);
        }
    };
    return rp(options);
}

module.exports = parseUrl;