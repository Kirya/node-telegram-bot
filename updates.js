const Crawler = require("crawler");
const db = require('./db');

async function sendUpdates () {
    var c = new Crawler({
        maxConnections : 10,
        async callback(error, res, done) {
            if(error){
                console.log(error);
            }else{
                const $ = res.$;
                const itemSelector = $('.search-list__listing').find('.search-list__item:not(.common-b) script');
                const items = [];
                const userId = res.options.user;
                const currentTimeMinus2mins = new Date().getTime() - 60000 * 5;
                itemSelector.each((i, elem) => {
                    let obj = JSON.parse($(elem).text().match(/\{[^]+\}/gm));
                    obj.publicationDate = new Date(obj.lastUpdate).getTime();
                    let {publicationDate, url} = obj;
                    items.push({
                        date: publicationDate,
                        url
                    });
                });
                let checkNewCars = items.filter(car => car.date > currentTimeMinus2mins);
                if(checkNewCars.length) {
                    for(let i = 0; i < checkNewCars.length; i++) {
                        bot.sendMessage(userId, checkNewCars[i].url);
                    }
                } //else {
                    //bot.sendMessage(userId, 'Пусто !');
                //}
                
                console.log('update');
            }
            done();
        }
    });
    const users = await db.getUsers();
    for(user of users) {
        if(user.links.length) {
            let links = [];
            user.links.forEach(link => links.push({uri:link,user:user._id}));
            c.queue(links);
        }
    }
}

module.exports = sendUpdates;